﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

class Solution
{
    class Graph : Dictionary<int, List<int>>
    {

    }

    static void Main(string[] args)
    {
#if TRACE
        Console.SetIn(new StringReader(@"3
1 2
1 3
3 4")); // 3
#endif

        Graph graph = new Graph();

        int n = int.Parse(Console.ReadLine()); // the number of relationships of influence
        for (int i = 0; i < n; i++)
        {
            string[] inputs = Console.ReadLine().Split(' ');
            int x = int.Parse(inputs[0]); // a relationship of influence between two people (x influences y)
            int y = int.Parse(inputs[1]);

            if(graph.ContainsKey(x))
            {
                graph[x].Add(y);
            }
            else
            {
                List<int> list = new List<int>();
                list.Add(y);
                graph.Add(x, list);
            }
        }

        int max = 0;
        foreach (int node in graph.Keys)
        {
            max = Math.Max(max, mesure(node, graph, 1));
        }


        // The number of people involved in the longest succession of influences
        Console.WriteLine(max);

#if TRACE
        Console.Error.WriteLine("Done! Push a key");
        Console.ReadKey();
#endif
    }

    private static int mesure(int node, Graph graph, int depth)
    {
        int max = 0;
        if (graph.ContainsKey(node))
        {
            foreach (int child in graph[node])
            {
                max = Math.Max(max, mesure(child, graph, depth + 1));
            }
        }

        return Math.Max(depth, max);
    }
}