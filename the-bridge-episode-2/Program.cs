﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace the_bridge_episode_2
{
    enum Command
    {
        SPEED, SLOW, JUMP, WAIT, UP, DOWN
    }

    class Motorbike
    {
        public int x, y;
        public int speed;
        public bool active;

        public int nextHole;
        public string move;
    }

    class Program
    {
        static void Main(string[] args)
        {
#if TRACE
            Console.SetIn(new StringReader(""));
#endif

            int motorbikes = int.Parse(Console.ReadLine()); // the amount of motorbikes to control
            int minSurvivors = int.Parse(Console.ReadLine()); // the minimum amount of motorbikes that must survive
            bool[][] lines = new bool[][] {
                ReadMapLine(Console.ReadLine()),
                ReadMapLine(Console.ReadLine()),
                ReadMapLine(Console.ReadLine()),
                ReadMapLine(Console.ReadLine())
            };
            int mapWidth = lines[0].Length;

            Motorbike[] bikes = new Motorbike[motorbikes];

            // game loop
            while (true)
            {
                int speed = int.Parse(Console.ReadLine()); // the motorbikes' speed
                for (int i = 0; i < motorbikes; i++)
                {
                    string[] inputs = Console.ReadLine().Split(' ');
                    Motorbike bike = bikes[i];
                    bike.x = int.Parse(inputs[0]); // x coordinate of the motorbike
                    bike.y = int.Parse(inputs[1]); // y coordinate of the motorbike
                    bike.active = int.Parse(inputs[2]) == 1; // indicates whether the motorbike is activated "1" or detroyed "0"
                    bike.speed = speed;

                    for(int x = bikes[i].x + 1; x < mapWidth; x++)
                    {
                        if (lines[bike.y][x])
                            bike.nextHole = x;
                    }
                }





                // A single line containing one of 6 keywords: SPEED, SLOW, JUMP, WAIT, UP, DOWN.
                Console.WriteLine("SPEED");
            }
        }

        static bool[] ReadMapLine(string strLine)
        {
            bool[] line = new bool[strLine.Length];
            for (int i = 0; i < strLine.Length; i++)
                line[i] = strLine[i] == '0';
            return line;
        }
    }
}
