﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * <see cref="https://www.codingame.com/ide/puzzle/aneo"/>
 * 
 *  <summary>
 *  Input
 *  Line 1: An integer speed for the maximum speed allowed on the portion of the road (in km / h).
 *
 *  Line 2: An integer lightCount for the number of traffic lights on the road.
 *
 *  lightCount next lines:
 *  - An integer distance representing the distance of the traffic light from the starting point (in meters).
 *  - An integer duration representing the duration of the traffic light on each color.
 *
 *  A traffic light alternates a period of duration seconds in green and then duration seconds in red.
 *  All traffic lights turn green at the same time as you enter the area.
 *  
 *  Output
 *  Line 1: The integer speed (km / h) as high as possible that cross all the green lights without committing speeding.
 *  
 *  Constraints
 *  1 ≤ speed ≤ 200
 *  1 ≤ lightCount ≤ 9999
 *  1 ≤ distance ≤ 99999
 *  1 ≤ duration ≤ 9999
 * </summary>
 * <example>
 * Input
 *  speed= 50
 *  lightCount= 1
 *  200 15
 * Output: 50
 * </example>
 * <example>
 * Input
 *  speed= 50
 *  lightCount= 1
 *  200 10
 * Output: 36
 * </example>
 * <example>
 * Input
 *  speed= 90
 *  lightCount= 3
 *  300 30
 *  1500 30
 *  3000 30
 * Output: 90
 * </example>
 **/
class Solution
{
    static void Main(string[] args)
    {
#if TRACE
        Console.SetIn(new StringReader(
@"90
3
300 10
1500 10
3000 10"));
#endif

        int speedKmH = int.Parse(Console.ReadLine()); // km/s
        Console.Error.WriteLine("maxSpeed= " + speedKmH);
        int lightCount = int.Parse(Console.ReadLine());
        Console.Error.WriteLine("lightCount= " + lightCount);
        int[] distances = new int[lightCount];
        int[] durations = new int[lightCount];
        for (int i = 0; i < lightCount; i++)
        {
            string[] inputs = Console.ReadLine().Split(' ');
            distances[i] = int.Parse(inputs[0]);
            durations[i] = int.Parse(inputs[1]);

            Console.Error.WriteLine(distances[i] + " " + durations[i]);
        }

        int outputSpeed = -1;
        for(int speed = speedKmH; speed > 0; speed--)
        {
            float speedMS = convertMS(speed);
            Console.Error.WriteLine(" Test for {0}km/h = {1}m/s", speed, speedMS);

            bool valid = true;
            for(int i = 0; i < lightCount; i++)
            {
                if (!testGreen(speedMS, distances[i], durations[i]))
                {
                    valid = false;
                    break;
                }
            }

            if (valid)
            {
                outputSpeed = speed;
                break;
            }
        }

        Console.Error.WriteLine("Output= " + outputSpeed);
        Console.WriteLine(outputSpeed);

#if TRACE
        Console.ReadKey();
#endif
    }

    static float convertMS(int speedKmH) => speedKmH * 1000f / 3600f;
    static int convertKmH(float speedMS) => (int)(speedMS / 1000f * 3600f);

    static bool testGreen(float speedMS, int distance, int duration)
    {
        float time = distance / speedMS;
        int aux = (int)(time / duration);
        bool ret = aux % 2 == 0;
        Console.Error.WriteLine("  testGreen s={0}, d={1}, a={2} | {3} = {4}", speedMS, distance, duration, aux, ret);
        return ret;
    }
}