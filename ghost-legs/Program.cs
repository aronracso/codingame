﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

class Solution
{
    static void Main(string[] args)
    {
        Console.SetIn(new StringReader(
@"7 7
A  B  C
|  |  |
|--|  |
|  |--|
|  |--|
|  |  |
1  2  3"));

        string[] inputs = Console.ReadLine().Split(' ');
        int W = int.Parse(inputs[0]);
        int H = int.Parse(inputs[1]);

        char[] STARTS = new char[(W / 3) + 1];
        char[] ENDS = new char[(W / 3) + 1];
        bool[][] CONNECTS = new bool[H-2][];

        //First line (STARTS)
        string line = Console.ReadLine();
        Console.Error.Write("Starts: ");
        for (int i = 0; i < W; i += 3)
        {
            STARTS[i / 3] = line[i];
            Console.Error.Write(line[i]);
        }
        Console.Error.WriteLine();

        //Middle lines, connects
        Console.Error.WriteLine("Connects:");
        for (int i = 0; i < (H - 2); i++)
        {
            line = Console.ReadLine();
            CONNECTS[i] = new bool[W / 3];

            for (int j = 1; j < line.Length; j += 3)
            {
                if (line[j] == '-')
                {
                    CONNECTS[i][j/3] = true;
                    Console.Error.WriteLine(i + ", " + j/3);
                }
            }
        }

        //Last line (ENDS)
        line = Console.ReadLine();
        Console.Error.Write("Ends: ");
        for (int i = 0; i < W; i += 3)
        {
            ENDS[i / 3] = line[i];
            Console.Error.Write(line[i]);
        }
        Console.Error.WriteLine();

        for(int i = 0; i < STARTS.Length; i++)
        {
            int result = Iterate(i, 0, CONNECTS);
            Console.WriteLine("" + STARTS[i] + ENDS[result]);
        }

        Console.ReadKey();
    }

    private static int Iterate(int column, int row, bool[][] CONNECTS)
    {
        if (CONNECTS.Length <= row)
            return column;

        int leftColumn = column - 1;
        if(leftColumn >= 0 && CONNECTS[row][leftColumn])
        {
            return Iterate(leftColumn, row + 1, CONNECTS);
        }
        else if(column < CONNECTS[row].Length && CONNECTS[row][column])
        {
            return Iterate(column + 1, row + 1, CONNECTS);
        }
        else
        {
            return Iterate(column, row + 1, CONNECTS);
        }
    }
}