﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// <see cref="https://www.codingame.com/ide/puzzle/bender-episode-2"/>
/// </summary>
class Solution
{
    static readonly char EXIT = 'E';


    class Node
    {
        public int Id { get; protected set; }
        public bool IsExit { get; set; }
        public int Money { get; set; }
        public List<Node> Doors { get; protected set; }

        public Node(int id)
        {
            Id = id;
            Doors = new List<Node>();
        }
    }



    static void Main(string[] args)
    {
        List<Node> nodes = new List<Node>();
        Node startRoom, exitRoom;

        int nLines = int.Parse(Console.ReadLine());
        startRoom = new Node(0);
        nodes.Add(startRoom);
        for (int i = 1; i < nLines; i++)
            nodes.Add(new Node(i));

        exitRoom = new Node(int.MaxValue)
        {
            IsExit = true
        };
        nodes.Add(exitRoom);

        for (int i = 0; i < nLines; i++)
        {
            Node node = nodes[i];

            string[] room = Console.ReadLine().Split(" ");
            int money = int.Parse(room[1]);
            node.Money = money;

            Node doorNode;
            if (room[2][0] == EXIT)
                doorNode = nodes[nodes.Count - 1];
            else
                doorNode = nodes[int.Parse(room[2])];
            nodes[i].Doors.Add(doorNode);

            if (room[3][0] == EXIT)
                doorNode = nodes[nodes.Count - 1];
            else
                doorNode = nodes[int.Parse(room[3])];
            nodes[i].Doors.Add(doorNode);
        }

        int totalMoney = 0;

        // Write an action using Console.WriteLine()
        // To debug: Console.Error.WriteLine("Debug messages...");

        Console.WriteLine(totalMoney);
    }
}