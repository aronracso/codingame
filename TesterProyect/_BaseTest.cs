﻿using NUnit.Framework;
using System;
using System.IO;
using System.Text;

namespace TesterProyect
{
    abstract class BaseTest
    {
        protected string[] testCases;

        internal BaseTest(string[] testCases)
        {
            this.testCases = testCases;
        }

        protected void TestHelper(int i)
        {
            TestHelper(testCases[i * 2], testCases[i * 2 + 1]);
        }

        protected void TestHelper(string input, string output)
        {
            StringBuilder sbOutput = new StringBuilder();
            Console.SetOut(new StringWriter(sbOutput));
            Console.SetIn(new StringReader(input));

            RunSolution();

            Assert.AreEqual(output + Environment.NewLine, sbOutput.ToString());
        }

        protected abstract void RunSolution();
    }
}
