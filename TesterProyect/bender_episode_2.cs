﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using bender_episode_2;

namespace TesterProyect
{
    [TestFixture]
    class Bender_episode_2 : BaseTest
    {
        private readonly static string[] TEST_CASES = new string[]
        {

        };

        public Bender_episode_2() : base(TEST_CASES)
        {

        }

        protected override void RunSolution()
        {
            Solution.Main(null);
        }

        [Test] public void Test0() { TestHelper(0); }
        //[Test] public void Test1() { TestHelper(1); }
        //[Test] public void Test2() { TestHelper(2); }
        //[Test] public void Test3() { TestHelper(3); }
        //[Test] public void Test4() { TestHelper(4); }
        //[Test] public void Test5() { TestHelper(5); }
        //[Test] public void Test6() { TestHelper(6); }
        //[Test] public void Test7() { TestHelper(7); }
        //[Test] public void Test8() { TestHelper(8); }
        //[Test] public void Test9() { TestHelper(9); }
        //[Test] public void Test10() { TestHelper(10); }
        //[Test] public void Test11() { TestHelper(11); }
        //[Test] public void Test12() { TestHelper(12); }
        //[Test] public void Test13() { TestHelper(13); }
        //[Test] public void Test14() { TestHelper(14); }
        //[Test] public void Test15() { TestHelper(15); }
        //[Test] public void Test16() { TestHelper(16); }
        //[Test] public void Test17() { TestHelper(17); }
        //[Test] public void Test18() { TestHelper(18); }
        //[Test] public void Test19() { TestHelper(19); }
        //[Test] public void Test20() { TestHelper(20); }
        //[Test] public void Test21() { TestHelper(21); }
        //[Test] public void Test22() { TestHelper(22); }
        //[Test] public void Test23() { TestHelper(23); }
    }

}
