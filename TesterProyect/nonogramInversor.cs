﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using nonogram_inversor;

namespace TesterProyect
{
    [TestFixture]
    class NonogramInversor : BaseTest
    {
        private readonly static string[] TEST_CASES = new string[]
        {
//0
@"5 5
1
3
2
5
1
1
1 3
3
1 1
1 1",
@"1 3
2
1 2
0
1 3
3 1
1
1 1
1 1 1
1 1 1",

//1
@"10 10
2
4
4
8
1 1
1 1
1 1 2
1 1 4
1 1 4
8
4
3 1
1 3
4 1
1 1
1 3
3 4
4 4
4 2
2",
@"7 1
6
6
1 1
1 1 6
1 1 6
2 2 2
1 2 1
1 2 1
2
6
3 3
3 3
3 2
3 5
3 3
1 2
2
3 1
1 7",

//2
@"15 15
3
4
1 4 3
1 4
7 5
2 4
4 4
2 6
4
1 4 3
1 4
7 2
2 4 1
4 3 1
2 7
1 1 1 1
1 3 1 3
5 5
1 2 1 2
1 1
2 2
2 2
2 2
2 2 1
3 4 3
3 6 4
2 5 4
2 6 1 1
1 2 1 1 1
1 1 1 1 3",
@"9 3
9 2
1 5 1
2 2 6
3
1 7 1
5 2
2 5
9 2
1 5 1
2 2 6
3 3
1 6 1
5 2
2 4
4 1 4 1 1
2 1 2 1 1
3 2
4 1 3 1
4 6 3
3 5 3
3 5 3
2 5 4
2 5 3
3 2
1 1
2 2
1 1 2 1
2 1 1 1 4
2 1 2 1 2",

//3
@"15 20
2 6 3
4 6 1
7 1
6 9 1
6 12
2 12 2
14 1 1
4 7 2 2
3 8 3
7 6 4
3 2 4 1 2
2 3 3 1 1
1 2 3 2 2 2
1 8 1 1 3
1 2 2 2 2
1 1 1 1
2 2 1 1
7 2 1
8 3
2 2 2 1
1 8 1
2 3 2 3
1 4 4
1 10
2 9 1
1 8 1
1 9
11 3
8 4 1
7 2 1
7 2 1
5 4 3
2 2 1 2 1 1
1 1 2 4 2
2 5 4",
@"5 2 2
6 2 1
10 1 1
3 1
1 1
2 1 1
2 1 1
2 1 1 1
1 1 3 1
1 2
3 2 1 1 1
1 4 3 1 1
1 2 3 1 1
1 1 1 1 1 1
1 2 4 1 3
3 5 2 1
3 3 1 1 1
3 1 1
3 1
3 1 1 2 1
2 2 1
2 1 2
1 3 2
1 2 1
1 1 1
1 3 1
1 4
1
1 1
3 1 1
1 3 1
1 1 1
1 1 1 1 1 1
1 1 1 1 1
1 1 2",


//4
@"20 20
1 1
1 2 1
3 2
1 1 1 6
3 4
3 2
3 1 2 2
1 2 1 2 3
2 3 3 4
4 2 9
7 9
4 2 9
3 1 9
3 2 8
4 5 3
10 1
10
10
9
3
1 6
1 8
1 10
3 3
3 1 1 1 3
15
8 5
7
1 9
12
6 4
8 3
2 7 3
2 1 6 3
4 6 2
3 6
9
7
3
4",
@"14 4
1 11 4
13 2
1 2 8
4 8 1
4 9 2
4 1 3 2 2
2 2 2 1 2 2
1 1 2 2 2
1 2 2
1 3
1 1 3
2 1 4
1 1 5
1 1 6
1 1 7
2 8
4 6
6 5
12 5
3 5 5
1 6 4
3 3 3
9 2 3
4 1 1 2 1 2
3 2
4 2 1
12 1
6 3 1
7 1
8 1 1
7 1 1
6 1 1
1 3 2 2
4 4
2 2 7
3 8
3 10
2 15
16",
        };

        public NonogramInversor() : base(TEST_CASES)
        {

        }

        protected override void RunSolution()
        {
            Solution.Main(null);
        }

        [Test] public void Test0() { TestHelper(0); }
        [Test] public void Test1() { TestHelper(1); }
        [Test] public void Test2() { TestHelper(2); }
        [Test] public void Test3() { TestHelper(3); }
        [Test] public void Test4() { TestHelper(4); }
        //[Test] public void Test5() { TestHelper(5); }
        //[Test] public void Test6() { TestHelper(6); }
        //[Test] public void Test7() { TestHelper(7); }
        //[Test] public void Test8() { TestHelper(8); }
        //[Test] public void Test9() { TestHelper(9); }
        //[Test] public void Test10() { TestHelper(10); }
        //[Test] public void Test11() { TestHelper(11); }
        //[Test] public void Test12() { TestHelper(12); }
        //[Test] public void Test13() { TestHelper(13); }
        //[Test] public void Test14() { TestHelper(14); }
        //[Test] public void Test15() { TestHelper(15); }
        //[Test] public void Test16() { TestHelper(16); }
        //[Test] public void Test17() { TestHelper(17); }
        //[Test] public void Test18() { TestHelper(18); }
        //[Test] public void Test19() { TestHelper(19); }
        //[Test] public void Test20() { TestHelper(20); }
        //[Test] public void Test21() { TestHelper(21); }
        //[Test] public void Test22() { TestHelper(22); }
        //[Test] public void Test23() { TestHelper(23); }
    }

}
