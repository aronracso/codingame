using System;
using NUnit.Framework;
using System.Text;
using System.IO;
using winamax_sponsored_contest;

namespace TesterProyect
{
    [TestFixture]
    class WinamaxSponsoredContestTest : BaseTest
    {
        private readonly static string[] TEST_CASES = new string[]
        {
//0
@"2 1
1H",
@">.",

//1
@"3 3
2.X
..H
.H1",
@"v..
v..
>.^",

//2
@"5 5
4..XX
.H.H.
...H.
.2..2
.....",
@"v....
v...<
v^..^
v^.^^
>>>^.",

//3
@"6 6
3..H.2
.2..H.
..H..H
.X.2.X
......
3..H..",
@">>>..v
.>>>.v
>>....
^..v..
^..v..
^.....",

//4
@"8 8
.XXX.5X.
X.4.X..X
X4..X3.X
X...X.X.
.X.X.H.X
X.HX...X
X..X.H.X
.XH.XXX.",
@"v<<<<<..
v.>>>>v.
vvv<<<v.
vvv...v.
vvv.>.v.
vv..^.v.
v>>>^.<.
>>......",

//5
@"8 8
.......4
....HH.2
..5.....
H....22X
.3XH.HXX
..X3.H.X
..XH....
H2X.H..3",
@"v<<<<<<<
v>>>..<<
v^>>>>>v
.^.v<<vv
.^..>.vv
v<<<^.<v
v...^<<<
.>>^.<<<",

//6
@"8 8
4H5.....
......3.
......H.
.....H3.
........
....HH..
.43.....
..H3H...",
@"v.>>>>>v
v^<<<<<v
v>>>>>.v
v^>>>.vv
v^^>>vvv
v^^^..vv
v^^^^<<v
>>.^.<<<",

//7
@"8 8
H..3..H.
......5.
.XXXXX.2
3X.H.X2.
.XXXXX.H
.....H..
H..3..H.
.2..H..3",
@".<<<...<
.v<<<<<^
.v.....^
vv..<.v.
vv..^.v.
v>>>^.<^
...>>>.^
.>>>...^",

//8
@"40 8
.XXX.5XX4H5............4H..3XXH.2.HX3...
XX4.X..X......3.....HH.2X.....5.....4XX.
X4..X3.X......H...5.....XXXXXXX2.HX2..H.
X..XXXXX.....H3.H.X..22X3XXH.X2X...2HHXH
.X.X.H.X........X3XH.HXX.XXXXX.H..HX..2.
X.HX.X.X....HH....X3.H.X.....H..XXXX3...
X..X.H.X.43......XXH....HXX3..H.X2.HX2..
.XHXXXXX..H3H...H2X.H..3X2..HXX3H.2XXXXH",
@"v<<<<<..v.>>>>>vv<<<<<<<.<<<...<>>..>>>v
v.>>>>v.v^<<<<<vv>>>..<<.v<<<<<^v<<<<..v
vvv<<<v.v>>>>>.vv^>>>>>v.v.....^v..>>v.<
vvv...v.v^>>>.vv.^.v<<vvvv..<.v.v^<<....
vvv.>.v.v^^>>vvv.^..>.vvvv..^.v.>>..^<<^
vv..^.v.v^^^..vvv<<<^.<vv>>>^.<^....>>>^
v>>>^.<.v^^^^<<vv...^<<<...>>>.^.>>..>>v
>>......>>.^.<<<.>>^.<<<.>>>...^.<<.....",

//9
@"3 3
2..
...
.H.",
@"v..
v..
>..",

//10
@"6 6
.3H.X2
X.....
2XHX..
..XHXH
.H..3X
.X3...",
@".v.<<<
.vv<<.
vv..^.
v>>.^.
>...^^
..>>>^",
        };

        public WinamaxSponsoredContestTest() : base(TEST_CASES)
        {

        }

        protected override void RunSolution()
        {
            Solution.Main(null);
        }

        [Test] public void Test0() { TestHelper(0); }
        [Test] public void Test1() { TestHelper(1); }
        [Test] public void Test2() { TestHelper(2); }
        [Test] public void Test3() { TestHelper(3); }
        [Test] public void Test4() { TestHelper(4); }
        [Test] public void Test5() { TestHelper(5); }
        [Test] public void Test6() { TestHelper(6); }
        [Test] public void Test7() { TestHelper(7); }
        [Test] public void Test8() { TestHelper(8); }
        [Test] public void Test9() { TestHelper(9); }
        [Test] public void Test10() { TestHelper(10); }
        //[Test] public void Test11() { TestHelper(11); }
        //[Test] public void Test12() { TestHelper(12); }
        //[Test] public void Test13() { TestHelper(13); }
        //[Test] public void Test14() { TestHelper(14); }
        //[Test] public void Test15() { TestHelper(15); }
        //[Test] public void Test16() { TestHelper(16); }
        //[Test] public void Test17() { TestHelper(17); }
        //[Test] public void Test18() { TestHelper(18); }
        //[Test] public void Test19() { TestHelper(19); }
        //[Test] public void Test20() { TestHelper(20); }
        //[Test] public void Test21() { TestHelper(21); }
        //[Test] public void Test22() { TestHelper(22); }
        //[Test] public void Test23() { TestHelper(23); }
    }
}