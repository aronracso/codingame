﻿using System;

class Solution
{
    static void Main(string[] args)
    {
        int n = int.Parse(Console.ReadLine()); // the number of temperatures to analyse
        string[] inputs = Console.ReadLine().Split(' ');

        int closest = int.MaxValue;
        int closestTemp = 0;
        for (int i = 0; i < n; i++)
        {
            int temp = int.Parse(inputs[i]); // a temperature expressed as an integer ranging from -273 to 5526

            int absTemp = Math.Abs(temp);
            if(absTemp < closest)
            {
                closest = absTemp;
                closestTemp = temp;
            }
            else if(absTemp == closest)
            {
                closestTemp = Math.Max(temp, closestTemp);
            }
        }

        Console.WriteLine(closestTemp);
    }
}