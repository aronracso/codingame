using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

class Player
{
    class Floor
    {
        public bool[] Elevators { get; protected set; }
        public bool[] Blocks { get; protected set; }

        public Floor(int width)
        {
            Elevators = new bool[width];
            Blocks = new bool[width];
            for(int i = 0; i < width; i++)
            {
                Elevators[i] = false;
                Blocks[i] = false;
            }
        }
    }

    class Map
    {
        public int MaxRounds { get; protected set; }
        public int TotalClones { get; protected set; }
        public int NumberFloors { get; protected set; }
        public int Width { get; protected set; }
        public int ExitFloor { get; protected set; }
        public int ExitPosition { get; protected set; }
        public Floor[] Floors { get; protected set; }

        public void InitFromConsole()
        {
            string line = Console.ReadLine();
            Console.Error.WriteLine(line);
            string[] inputs = line.Split(' ');

            NumberFloors = int.Parse(inputs[0]); // number of floors
            Width = int.Parse(inputs[1]); // width of the area
            MaxRounds = int.Parse(inputs[2]); // maximum number of rounds
            ExitFloor = int.Parse(inputs[3]); // floor on which the exit is found
            ExitPosition = int.Parse(inputs[4]); // position of the exit on its floor
            TotalClones = int.Parse(inputs[5]); // number of generated clones
            int nbAdditionalElevators = int.Parse(inputs[6]); // ignore (always zero)
            int nbElevators = int.Parse(inputs[7]); // number of elevators

            Floors = new Floor[NumberFloors];
            for (int i = 0; i < Floors.Length; i++)
            {
                Floors[i] = new Floor(Width);
            }

            for (int i = 0; i < nbElevators; i++)
            {
                line = Console.ReadLine();
                Console.Error.WriteLine(line);
                inputs = line.Split(' ');
                int elevatorFloor = int.Parse(inputs[0]); // floor on which this elevator is found
                int elevatorPos = int.Parse(inputs[1]); // position of the elevator on its floor

                Floors[elevatorFloor].Elevators[elevatorPos] = true;
            }
        }

        public void DebugPrint()
        {
            //Console.Error.WriteLine(string.Format("NumberFloors = {0}, Width = {1}", NumberFloors, Width));
            //Console.Error.WriteLine(string.Format("ExitFloor = {0}, ExitPosition = {1}", ExitFloor, ExitPosition));

            Console.Error.Write(" MAP) [");
            for (int x = 0; x < Width; x++)
            {
                Console.Error.Write(x % 10);
            }
            Console.Error.WriteLine("]");

            for (int y = Floors.Length - 1; y >= 0; y--)
            {
                Console.Error.Write(string.Format("{0:0000}) [", y));
                for (int x = 0; x < Width; x++)
                {
                    if(y == ExitFloor && x == ExitPosition)
                        Console.Error.Write("E");
                    else if (Floors[y].Elevators[x])
                        Console.Error.Write("^");
                    else if (Floors[y].Blocks[x])
                        Console.Error.Write("#");
                    else
                        Console.Error.Write(" ");
                }
                Console.Error.WriteLine("]");
            }
        }

        public bool CanContinue(int cloneFloor, int clonePos, string direction)
        {
            Floor floor = Floors[cloneFloor];

            //Wait in the elevator
            if (floor.Elevators[clonePos])
            {
                Console.Error.WriteLine("ELEVATOR!");
                return true;
            }

            int delta = direction == "LEFT" ? -1 : 1;
            clonePos += delta;
            while (0 <= clonePos && clonePos < Width)
            {
                Console.Error.Write("CHECK ({0},{1},{2}= {3}) ", cloneFloor, clonePos, direction, delta);

                if (cloneFloor == ExitFloor && clonePos == ExitPosition)
                {
                    Console.Error.WriteLine("EXIT!");
                    return true;
                }

                if (floor.Elevators[clonePos])
                {
                    Console.Error.WriteLine("ELEVATOR!");
                    return true;
                }

                if (floor.Blocks[clonePos])
                {
                    Console.Error.Write("BLOCK!");
                    delta = delta * (-1);
                }

                clonePos += delta;
                Console.Error.WriteLine(string.Format(" move to {0}", clonePos));
            }

            Console.Error.WriteLine("NO WAY");
            return false;
        }
    }

    static string Direction(int from, int to)
    {
        return from > to ? "LEFT" : "RIGHT";
    }

    static void Main(string[] args)
    {
        Map map = new Map();
        map.InitFromConsole();

        // game loop
        while (true)
        {
            string[] inputs = Console.ReadLine().Split(' ');
            int cloneFloor = int.Parse(inputs[0]); // floor of the leading clone
            int clonePos = int.Parse(inputs[1]); // position of the leading clone on its floor
            string direction = inputs[2]; // direction of the leading clone: LEFT or RIGHT

            map.DebugPrint();
            Console.Error.WriteLine(string.Format("Move: cloneFloor={0}, clonePos={1}, direction={2}", cloneFloor, clonePos, direction));

            // action: WAIT or BLOCK
            if (cloneFloor < 0 || map.CanContinue(cloneFloor, clonePos, direction))
            {
                Console.WriteLine("WAIT");
            }
            else
            {
                map.Floors[cloneFloor].Blocks[clonePos] = true;
                Console.WriteLine("BLOCK");
            }
        }
    }
}