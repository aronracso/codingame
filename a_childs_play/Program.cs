﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

class Solution
{
    private static readonly char START = 'O';
    //private static readonly char GROUND = '.';
    private static readonly char OBSTACLE = '#';

    private static readonly int[][] DELTAS = new int[][]
    {
        new int[] { 0, -1 }, //up
        new int[] { 1, 0 }, //right
        new int[] { 0, 1 }, //down
        new int[] { -1, 0 } //left
    };

    class State
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Dir { get; set; }

        public State(int x, int y, int dir)
        {
            X = x;
            Y = y;
            Dir = dir;
        }

        public override bool Equals(object obj)
        {
            State state = obj as State;
            return state != null &&
                   X == state.X &&
                   Y == state.Y &&
                   Dir == state.Dir;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + X.GetHashCode();
            hash = hash * 23 + Y.GetHashCode();
            hash = hash * 23 + Dir.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1} ({2})", X, Y, Dir);
        }
    }

    static void Main(string[] args)
    {
#if TRACE
        string test = @"12 6
987
...#........
...........#
............
............
..#O........
..........#.";
        string testSolution = "7 1";

        Console.SetIn(new StringReader(test));
#endif

        string[] inputs = Console.ReadLine().Split(' ');
        int WIDTH = int.Parse(inputs[0]);
        int HEIGHT = int.Parse(inputs[1]);

        string[] map = new string[HEIGHT];
        int x = 0, y = 0;
        int dir = 0;

        long MOVE_NUMBER = long.Parse(Console.ReadLine());
        for (int i = 0; i < HEIGHT; i++)
        {
            map[i] = Console.ReadLine();
            int startX = map[i].IndexOf(START);
            if (startX >= 0)
            {
                x = startX;
                y = i;
            }
        }

        Console.Error.WriteLine("Start in {0}, {1}", x, y);
        
        List<State> moves = new List<State>();
        Dictionary<State, int> cycleDetector = new Dictionary<State, int>();

        for (long i = 0; i < MOVE_NUMBER; i++)
        {
            //Try straight
            int tx, ty;
            ApplyDelta(dir, x, y, out tx, out ty);
            bool canMove = IsValid(map, WIDTH, HEIGHT, tx, ty);
            while (!canMove)
            {
                //Turn and try
                dir = (dir + 1) % DELTAS.Length;
                ApplyDelta(dir, x, y, out tx, out ty);
                canMove = IsValid(map, WIDTH, HEIGHT, tx, ty);
            }

            State state = new State(x, y, dir);
            if (cycleDetector.ContainsKey(state))
            {
                //Cycle Detected: Calculate final position
                //INFO: All cycles are complete
                long remaingMoves = MOVE_NUMBER - i - 1;
                int index = (int)(MOVE_NUMBER % moves.Count);
                state = moves[index];
                x = state.X;
                y = state.Y;
                break;
            }
            else
            {
                cycleDetector.Add(state, moves.Count);
                moves.Add(state);

                x = tx;
                y = ty;
            }
        }

        string solution = x + " " + y;
        Console.WriteLine(solution);

#if TRACE
        if (testSolution != solution)
            Console.WriteLine("WRONG!");
        Console.WriteLine("Press any key...");
        Console.ReadKey();
#endif
    }

    private static void ApplyDelta(int dir, int x, int y, out int tx, out int ty)
    {
        tx = x + DELTAS[dir][0];
        ty = y + DELTAS[dir][1];
    }

    private static bool IsValid(string[] map, int width, int height, int x, int y)
    {
        return IsInside(width, height, x, y) && map[y][x] != OBSTACLE;
    }

    private static bool IsInside(int width, int height, int x, int y)
    {
        return 0 <= x && x < width &&
            0 <= y && y < height;
    }
}