﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution
{
    static void Main(string[] args)
    {
        //Letter Width
        int L = int.Parse(Console.ReadLine());
        //Letter Height
        int H = int.Parse(Console.ReadLine());

        //Text to output
        string TEXT = Console.ReadLine();
        TEXT = TEXT.ToUpper();

        //Ascii letters reference
        string[] ROWS = new string[H];
        for (int i = 0; i < H; i++)
            ROWS[i] = Console.ReadLine();

        //Ascii alphabet given in ROWS
        const string ABC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ?";

        //SOLUTION
        for (int i = 0; i < H; i++) //for each text line
        {
            for(int t = 0; t < TEXT.Length; t++) //for each letter in text
            {
                char oLetter = TEXT[t];
                int oIndex = GetLetterIndex(oLetter, ABC);
                WriteLetterRow(oIndex, ROWS[i], L);
            }

            Console.WriteLine();
        }
    }

    private static int GetLetterIndex(char letter, string ABC)
    {
        for(int i = 0; i < ABC.Length; i++)
        {
            if (ABC[i] == letter)
                return i;
        }

        return ABC.Length - 1;
    }

    private static void WriteLetterRow(int oIndex, string row, int L)
    {
        for (int i = oIndex * L; i < (oIndex * L) + L; i++)
            Console.Write(row[i]);
    }
}