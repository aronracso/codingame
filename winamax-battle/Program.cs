﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * <see cref="https://www.codingame.com/ide/puzzle/winamax-battle"/>
 * 
 * Input
 * 
 * Line 1: the number N of cards for player one.
 * 
 * N next lines: the cards of player one.
 * 
 * Next line: the number M of cards for player two.
 * 
 * M next lines: the cards of player two.
 * Output
 * 
 *     If players are equally first: PAT
 *     Otherwise, the player number (1 or 2) followed by the number of game rounds separated by a space character. A war or a succession of wars count as one game round.
 * 
 * Constraints
 * 0 &lt; N, M &lt; 1000
 * 
 * CARD VALUES: 2, 3, 4, 5, 6, 7, 8, 9, 10, J, Q, K, A
 * CARD SUIT:   D, H, C, S
 * 
 * <example>
 * Input:
3
AD
KC
QC
3
KH
QS
JC
 * Output: 1 3
 * </example>
 * 
 **/
class Solution
{
    class Deck
    {
        protected LinkedList<int> deck = new LinkedList<int>();

        public bool isEmpty()
        {
            return deck.Count == 0;
        }

        public int getSize()
        {
            return deck.Count;
        }

        public int removeCard()
        {
            int card = deck.First();
            deck.RemoveFirst();
            return card;
        }

        public void insertCards(params int[] cards)
        {
            foreach(int card in cards)
                deck.AddLast(card);
        }

        public void insertCards(Deck otherDeck)
        {
            while(otherDeck.deck.Count > 0)
            {
                int card = otherDeck.removeCard();
                insertCards(card);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach(int card in deck)
                sb.Append(" ").Append(card);
            return sb.ToString();
        }
    }

    static void Main(string[] args)
    {
#if TRACE
//        Console.SetIn(new StringReader(
//@"3
//AD
//KC
//QC
//3
//KH
//QS
//JC"));

        Console.SetIn(new StringReader(
@"26
10H
KD
6C
10S
8S
AD
QS
3D
7H
KH
9D
2D
JC
KS
3S
2S
QC
AC
JH
7D
KC
10D
4C
AS
5D
5S
26
2H
9C
8C
4S
5C
AH
JD
QH
7C
5H
4H
6H
6S
QD
9H
10C
4D
JS
6D
3H
8H
3C
7S
9S
8D
2C"));
#endif

        Deck player1 = new Deck();
        Deck player2 = new Deck();
        Deck tableP1 = new Deck();
        Deck tableP2 = new Deck();

        Console.Error.WriteLine("INPUT");
        int n = int.Parse(Console.ReadLine()); // the number of cards for player 1
        Console.Error.WriteLine(n);
        for (int i = 0; i < n; i++)
        {
            string cardp1 = Console.ReadLine(); // the n cards of player 1
            Console.Error.WriteLine(cardp1);
            player1.insertCards(getCardValue(cardp1));
        }
        int m = int.Parse(Console.ReadLine()); // the number of cards for player 2
        Console.Error.WriteLine(m);
        for (int i = 0; i < m; i++)
        {
            string cardp2 = Console.ReadLine(); // the m cards of player 2
            Console.Error.WriteLine(cardp2);
            player2.insertCards(getCardValue(cardp2));
        }
        Console.Error.WriteLine("END INPUT");


        int roundCount = 0;
        bool inWar = false;
        int winner = -1;
        do
        {
            if(!inWar)
                roundCount += 1;

            Console.Error.WriteLine("ROUND {0}", roundCount);

            Console.Error.WriteLine(" P1 DECK {0}", player1);
            Console.Error.WriteLine(" P2 DECK {0}", player2);


            int c1 = player1.removeCard();
            int c2 = player2.removeCard();

            Console.Error.WriteLine("   Players shows theirs cards: P1: {0}, P2: {1}", c1, c2);

            if (c1 > c2)
            {
                //Battle Winner - P1
                if (inWar)
                {
                    player1.insertCards(tableP1);
                    player1.insertCards(c1);
                    player1.insertCards(tableP2);
                    player1.insertCards(c2);
                    inWar = false;
                }
                else
                {
                    player1.insertCards(c1, c2);
                }

                Console.Error.WriteLine("   P1 wins the battle");

                if (player2.isEmpty())
                {
                    Console.Error.WriteLine("PLAYER 1 WINS!");
                    winner = 1;
                }
            }
            else if(c1 < c2)
            {
                //Battle Winner - P2
                if (inWar)
                {
                    player2.insertCards(tableP1);
                    player2.insertCards(c1);
                    player2.insertCards(tableP2);
                    player2.insertCards(c2);
                    inWar = false;
                }
                else
                {
                    player2.insertCards(c1, c2);
                }

                Console.Error.WriteLine("   P2 wins the battle");

                if (player1.isEmpty())
                {
                    Console.Error.WriteLine("PLAYER 2 WINS!");
                    winner = 2;
                }
            }
            else
            {
                Console.Error.WriteLine("   P1 and P2 are equals");
                inWar = true;

                //Equall
                if (player1.getSize() < 3 || player2.getSize() < 3)
                {
                    Console.Error.WriteLine("EQUALS: Not enought cards to continue");
                    winner = 0;
                }
                else
                {
                    tableP1.insertCards(c1);
                    tableP1.insertCards(player1.removeCard());
                    tableP1.insertCards(player1.removeCard());
                    tableP1.insertCards(player1.removeCard());

                    tableP2.insertCards(c2);
                    tableP2.insertCards(player2.removeCard());
                    tableP2.insertCards(player2.removeCard());
                    tableP2.insertCards(player2.removeCard());
                }
            }

        } while (winner < 0);
        
        if(winner == 0)
            Console.WriteLine("PAT");
        else
            Console.WriteLine(winner + " " + roundCount);

#if TRACE
        Console.ReadKey();
#endif
    }

    static int getCardValue(string card)
    {
        card = card.Substring(0, card.Length - 1); //remove last letter (suit)
        if (int.TryParse(card, out int value))
        {
            if (value < 2 || value > 10)
                throw new ArgumentException("Card value " + value + " is not valid");

            return value;
        }
        else
        {
            switch (card)
            {
                case "J": return 11;
                case "Q": return 12;
                case "K": return 13;
                case "A": return 14;
                default:
                    throw new ArgumentException("Card value " + card + " is not valid");
            }
        }
    }
}