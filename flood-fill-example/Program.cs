using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace flood_fill_example
{
    class Solution
    {
        class Point
        {
            public int X { get; set; }
            public int Y { get; set; }

            public Point() { }

            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }

            public override bool Equals(object obj)
            {
                Point other = obj as Point;
                if (other == null)
                    return false;
                return X == other.X && Y == other.Y;
            }

            public override int GetHashCode()
            {
                return ToString().GetHashCode();
            }

            public override string ToString()
            {
                return string.Format("{0},{1}", X, Y);
            }
        }

        class MoveList : Dictionary<Point, Cell>
        {

        }

        class Cell
        {
            public int Id { get; set; }
            public char Value { get; set; }

            public Cell(int id, char desc)
            {
                Id = id;
                Value = desc;
            }

            public Cell(Cell cell)
            {
                Id = cell.Id;
                Value = cell.Value;
            }

            public override string ToString()
            {
                return string.Format("{0} (id={1})", Value, Id);
            }
        }

        class StringMap
        {
            public int W { get; private set; } = 0;
            public int H { get; private set; } = 0;
            private Cell[,] map;

            private static readonly Point[] DELTAS = new Point[]
            {
                new Point(0,1),
                new Point(0,-1),
                new Point(1, 0),
                new Point(-1,0)
            };

            public void ReadFromConsole()
            {
                W = int.Parse(Console.ReadLine());
                H = int.Parse(Console.ReadLine());

                map = new Cell[W, H];
                int id = 1;

                for (int y = 0; y < H; y++)
                {
                    string line = Console.ReadLine();
                    for (int x = 0; x < line.Length; x++)
                    {
                        if (line[x] == UNREACHABLE || line[x] == REACHABLE)
                        {
                            map[x, y] = new Cell(-1, line[x]);
                        }
                        else
                        {
                            map[x, y] = new Cell(id++, line[x]);
                        }
                    }
                }
            }

            public void SearchMoves(MoveList moves)
            {
                for (int x = 0; x < W; x++)
                {
                    for (int y = 0; y < H; y++)
                    {
                        SearchMoves(x, y, moves);
                    }
                }
            }

            public void SearchMoves(int x, int y, MoveList moves)
            {
                Cell cell = GetCell(x, y);
                if (cell.Value == UNREACHABLE || cell.Value == REACHABLE)
                    return;

                foreach(Point delta in DELTAS)
                {
                    Point tp = new Point(x + delta.X, y + delta.Y);

                    if(IsInside(tp.X, tp.Y))
                    {
                        Cell tcell = GetCell(tp.X, tp.Y);
                        if (tcell.Value == REACHABLE)
                        {
                            if (moves.ContainsKey(tp))
                            {
                                if (moves[tp].Id != cell.Id)
                                {
                                    moves[tp].Id = 0;
                                    moves[tp].Value = SPOTMARK;
                                }
                            }
                            else
                            {
                                moves.Add(tp, new Cell(cell));
                            }
                        }
                    }
                }
            }

            public void ApplyMoves(MoveList moves)
            {
                foreach(KeyValuePair<Point, Cell> pair in moves)
                {
                    SetCell(pair.Key.X, pair.Key.Y, pair.Value);
                }
            }

            public void SetCell(int x, int y, Cell value)
            {
                map[x, y] = value;
            }

            public Cell GetCell(int x, int y)
            {
                return map[x, y];
            }

            public int TraslateCoords(int x, int y)
            {
                return x + (y * (W + Environment.NewLine.Length));
            }

            public bool IsInside(int x, int y)
            {
                return 0 <= x && x < W
                    && 0 <= y && y < H;
            }

            public override string ToString()
            {
                return string.Format("StringMap (w={0}, h={1}){2}{3}{2}", W, H, Environment.NewLine, ToStringSolution());
            }

            public string ToStringSolution()
            {
                StringBuilder sb = new StringBuilder();
                for(int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        sb.Append(map[x, y].Value);
                    }
                    sb.AppendLine();
                }
                return sb.Remove(sb.Length - Environment.NewLine.Length, Environment.NewLine.Length).ToString();
            }
        }

        private static readonly char REACHABLE = '.';
        private static readonly char UNREACHABLE = '#';
        private static readonly char SPOTMARK = '+';

        static void Main(string[] args)
        {
#if TRACE
            int testnum = 3;
            Console.SetIn(new StringReader(_test[testnum]));
#endif

            StringMap map = new StringMap();
            map.ReadFromConsole();

            int i = 0;
            MoveList moves = new MoveList();
            do
            {
                moves.Clear();

                Console.Error.WriteLine("{0}) Current:", i++);
                Console.Error.WriteLine(map.ToString());

                map.SearchMoves(moves);
                map.ApplyMoves(moves);
            } while (moves.Count > 0);

            Console.WriteLine(map.ToStringSolution());

#if TRACE
            Console.Error.WriteLine("SOLUTION: " + (map.ToStringSolution() == _solution[testnum]));

            Console.Error.WriteLine("Done! Push a key");
            Console.ReadKey();
#endif
        }

#if TRACE

        //TEST:

        private static string[] _test = new string[]
        {
@"5
4
...#.
A#...
#..B.
.....",
@"10
10
..#.#...##
#..#.#....
.........#
..#..#..#.
.......#..
..#.JEDI.#
..#.....#.
.....#..#.
..........
..........",
@"12
12
#....##..#.#
#.#..#..5..#
...#........
......##....
......#..#..
1...........
............
.#.2#.......
...#..3.#.#.
....#.....#.
....#.....#.
...4...#....",
@"25
25
....#...........KYLO...#.
#.....#...#..#........#.#
..REY..........#..#......
#...#..#.#........##.#.#.
..#............#.........
......#.........#......#.
.#........#........#...#.
#..HAN...................
......#....#.##.....##.#.
##..#....#.#....#..#.....
..............###......#.
#.....#.........#...##...
...##..##.......#........
.#.###..#.....#.#.....#.#
....#...FN2187.....#.....
..#.........#....#.#.....
...........#...#.#...#...
.#...##...##.....#..#..#.
..................#...##.
.....#.#....##.......#..#
..........#........#.#...
.#.............#......#.#
...#.#.#.###..#..#.....##
#.#........###.......#...
..##.LEIA......#..#.##.##",
@"3
1
A.A"
        };

        private static string[] _solution = new string[]
        {
@"AA+#B
A#BBB
#BBBB
BBBBB",
@"JJ#.#DDD##
#JJ#J#DDDD
JJJJJ+DDD#
JJ#JJ#DD#I
JJJJJED#II
JJ#JJEDII#
JJ#JJEDI#I
JJJJJ#DI#I
JJJJJ+DIII
JJJJJ+DIII",
@"#11+5##55#5#
#1#55#55555#
111#55555555
111225##5555
11122+#55#55
11122+335555
11222+333333
1#22#3333333
122#3333#3#3
1++4#33333#3
4444#+3333#3
444444+#3333",
@"RRRE#YYYY+KKKKKKKYLOOOO#.
#RREYY#YYY#KK#KKKYLOOO#O#
RRREYYYYYYY+KKK#KY#OOOOOO
#RRE#YY#Y#Y+KKKKKY##O#O#O
RR#E++++++++KKK#KYY+OOOOO
++HHAN#NNNNN+KKK#YY+OOO#O
+#HHANNNNN#N++KYYYY#OOO#O
#HHHANNNNNNN++++YYY+OOOOO
HHHHAN#NNN+#8##7+YY+##O#O
##HH#NNNN#2#8777#++#7+OOO
HHHH+NNNNN2187###77777+#+
#HHH+N#NNN218777#777##777
HHH##+F##N218777#77777777
H#H###FF#N2187#7#77777#7#
+++F#FFFFN218777777#77777
++#FFFFFFN21#7777#7#77777
FFFFFFFFFN2#777#7#777#777
F#FFF##FFN##77777#77#77#7
LLLLL++FFNN+777777#777##7
LLLLL#E#+NN+##7777777#..#
LLLLLEE+AA#AA777777#7#...
L#LLLEE+AAAAAA7#777777#.#
LLL#L#E#A###AA#AA#AAAAA##
#L#LLLEIAAA###AAAAAAA#AAA
LL##LLEIAAAAAAA#AA#A##A##",
@"A+A"
        };

#endif
    }
}