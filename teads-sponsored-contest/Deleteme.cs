﻿//using System;
//using System.IO;
//using System.Text;
//using System.Collections;
//using System.Collections.Generic;


//class Solution
//{

//    static List<int> GetNextNodes(bool[][] nodes, int node)
//    {
//        List<int> ret = new List<int>();

//        for (int i = 0; i < nodes.Length; i++)
//        {
//            if (nodes[node][i])
//                ret.Add(i);
//        }

//        return ret;
//    }

//    static TextReader GetReader()
//    {
//        /*
//        return new StringReader(
//"13\r\n" +
//"6 8\r\n" +
//"15 1\r\n" +
//"1 14\r\n" +
//"0 3\r\n" +
//"15 13\r\n" +
//"9 15\r\n" +
//"2 5\r\n" +
//"14 10\r\n" +
//"4 9\r\n" +
//"7 2\r\n" +
//"8 7\r\n" +
//"3 4\r\n" +
//"1 6\r\n");*/

//        return Console.In;
//    }

//    static bool[][] ReadInput()
//    {
//        TextReader reader = GetReader();

//        string sn = reader.ReadLine();
//        int n = int.Parse(sn); // the number of adjacency relations
//        Console.Error.WriteLine(n);

//        int maxNode = 0;
//        List<int[]> relationList = new List<int[]>();
//        for (int i = 0; i < n; i++)
//        {
//            string[] inputs = reader.ReadLine().Split(' ');
//            int xi = int.Parse(inputs[0]); // the ID of a person which is adjacent to yi
//            int yi = int.Parse(inputs[1]); // the ID of a person which is adjacent to xi

//            //Console.Error.WriteLine(xi + " " + yi);
//            relationList.Add(new int[] { xi, yi });
//            maxNode = Math.Max(maxNode, xi);
//            maxNode = Math.Max(maxNode, yi);
//        }

//        maxNode += 1;
//        Console.Error.WriteLine("MaxNode=" + maxNode);

//        //Relation matrix
//        bool[][] nodes = new bool[maxNode][];
//        for (int i = 0; i < maxNode; i++)
//        {
//            nodes[i] = new bool[maxNode];
//        }

//        //Fill relations
//        foreach (int[] relation in relationList)
//        {
//            nodes[relation[0]][relation[1]] = true;
//            nodes[relation[1]][relation[0]] = true;
//        }

//        Console.Error.WriteLine("InputReaded");
//        return nodes;
//    }

//    static void Main(string[] args)
//    {
//        bool[][] nodes = ReadInput();

//        //Let's do the work
//        int bestSolution = int.MaxValue;

//        //Foreach node, find steps needed
//        for (int nodeChecked = 0; nodeChecked < nodes.Length; nodeChecked++)
//        {
//            //Console.Error.WriteLine("nodeChecked=" + nodeChecked + ", bestSol=" + bestSolution);

//            Queue<int> nextStepQueue = new Queue<int>();
//            nextStepQueue.Enqueue(nodeChecked);

//            int visitedCount = 1;
//            bool[] visited = new bool[nodes.Length];

//            int steps = 0;

//            while (nextStepQueue.Count > 0 && steps < bestSolution && visitedCount < nodes.Length)
//            {
//                steps += 1;
//                Queue<int> currentStepQueue = nextStepQueue;
//                nextStepQueue = new Queue<int>();

//                while (currentStepQueue.Count > 0 && visitedCount < nodes.Length)
//                {
//                    int node = currentStepQueue.Dequeue();
//                    if (visited[node])
//                        continue;

//                    visited[node] = true;

//                    List<int> next = GetNextNodes(nodes, node);

//                    //Fix: not all nodes are connected
//                    if (next.Count == 0 && node == nodeChecked)
//                        steps = int.MaxValue;

//                    foreach (int visitedNode in next)
//                    {
//                        if (visited[visitedNode])
//                            continue;

//                        visitedCount += 1;
//                        nextStepQueue.Enqueue(visitedNode);
//                        //Console.Error.WriteLine("   toVisitNode=" + visitedNode + " (visited=" + visitedCount + ")");
//                    }
//                }

//                //Console.Error.WriteLine("   step=" + steps + ", visited=" + visitedCount + ", inQueue=" + nextStepQueue.Count);
//            }

//            bestSolution = Math.Min(bestSolution, steps);
//        }

//        Console.WriteLine(bestSolution);
//        //Console.ReadKey();
//    }
//}