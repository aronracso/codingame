﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace two_player_game_on_a_calculator
{
    class Solution
    {
        /// <summary>
        /// KEYBOARD
        /// 7  8  9
        /// 4  5  6
        /// 1  2  3
        /// </summary>
        static readonly int[][] OPTIONS =
        {
            null, //0 (null)
            new int[] { 2, 4 ,5 }, //1
            new int[] { 1, 3, 4, 5 ,6 }, //2
            new int[] { 2, 5 ,6 }, //3
            new int[] { 1, 2, 5, 7, 8 }, //4
            new int[] { 1, 2, 3, 4, 6, 7, 8 ,9 }, //5
            new int[] { 2, 3, 5, 8 ,9 }, //6
            new int[] { 4, 5 ,8 }, //7
            new int[] { 4, 5, 6, 7 ,9 }, //8
            new int[] { 5, 6, 8 } //9
        };

        static void Main(string[] args)
        {
#if TRACE
            //Console.SetIn(new StringReader("2")); //1 2
            Console.SetIn(new StringReader("9")); //1 7 8 9
            //Console.SetIn(new StringReader("35")); //1 3 7
            //Console.SetIn(new StringReader("256")); //1 3 7 9
#endif 

            int n = int.Parse(Console.ReadLine());
            Console.Error.WriteLine("N= {0}", n);

            StringBuilder solution = new StringBuilder();
            for (int op = 1; op < OPTIONS.Length; op++)
            {
                if(explore(n, op, true, 0))
                {
                    Console.Error.WriteLine("OPTION {0} WINS", op);
                    if (solution.Length > 0)
                        solution.Append(" ");
                    solution.Append(op);
                }
                else
                {
                    Console.Error.WriteLine("OPTION {0} LOSES", op);
                }
            }


            Console.WriteLine(solution);
#if TRACE
            Console.ReadKey();
#endif
        }

        static bool explore(int n, int op, bool player, int level)
        {
#if TRACE
            for (int i = 0; i < level; i++)
                Console.Error.Write(" ");
            Console.Error.Write("Level {0}) {1} N= {2}, op= {3} => {4}", level, player ? "PLAYER" : "OTHER ", n, op, n - op);
#endif

            n = n - op;
            if (n < 0)
            {
#if TRACE
                Console.Error.WriteLine("{0}", !player ? " PLAYER WINS" : " PLAYER LOSES");
#endif
                return !player;
            }
            if (n == 0)
            {
#if TRACE
                Console.Error.WriteLine("{0}", player ? " PLAYER WINS" : " PLAYER LOSES");
#endif
                return player;
            }

#if TRACE
            Console.Error.WriteLine();
#endif

            foreach (int nextOp in OPTIONS[op])
            {
                if(explore(n, nextOp, !player, level+1))
                {
                    return player;
                }
            }

            return false;
        }
    }
}
