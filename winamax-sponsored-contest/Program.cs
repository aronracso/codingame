﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace winamax_sponsored_contest
{
    /// <summary>
    /// <see cref="https://www.codingame.com/ide/puzzle/winamax-sponsored-contest"/>
    /// 
    /// 
    /// Input
    ///
    /// Line 1: Two space separated integers width and height representing the size of the grid.
    ///
    /// Next height lines: One row of the grid containing width characters.
    /// Output
    /// All height rows of the solution grid, containing dots . for untouched cells or the characters v, &lt;, &gt;, ^ for arrows.
    /// Constraints
    /// 0 &lt; width &le; 1000
    /// 0 &lt; height &le; 1000
    ///
    /// 
    /// </summary>
    public class Solution
    {
        static readonly char NONE = '.';
        static readonly char WATER = 'X';
        static readonly char HOLE = 'H';
        static readonly char HOLE_FILLED = 'h';

        static readonly char DOWN = 'v';
        static readonly char LEFT = '<';
        static readonly char RIGHT = '>';
        static readonly char UP = '^';

        class Move
        {
            public int X { get; protected set; }
            public int Y { get; protected set; }
            public char C { get; protected set; }

            public Move(int x, int y, char c)
            {
                X = x;
                Y = y;
                C = c;
            }

            public override string ToString()
            {
                return string.Format("Move ({0}, {1}) [{2}]", X, Y, C);
            }
        }

        static readonly Move[] MOVES = new Move[]
        {
            new Move (1, 0, RIGHT),//>
            new Move (-1, 0, LEFT),//<
            new Move (0, 1, DOWN),   //^
            new Move (0, -1, UP),//v
        };

        class Ball
        {
            public string Id { get; protected set; }
            public int X { get; set; }
            public int Y { get; set; }
            public int ShotCount { get; set; }
            public Stack<Move> Moves { get; protected set; }

            public Ball(int x, int y, int shotCount)
            {
                Id = string.Format("{0}, {1}", x, y);
                X = x;
                Y = y;
                ShotCount = shotCount;
                Moves = new Stack<Move>();
            }

            //public Ball(Ball ball)
            //{
            //    X = ball.X;
            //    Y = ball.Y;
            //    ShotCount = ball.ShotCount;
            //}

            public override string ToString()
            {
                return string.Format("Ball[{3}] ({0}, {1}) shots={2}", X, Y, ShotCount, Id);
            }
        }

        class Map
        {
            public int Width { get; protected set; }
            public int Height { get; protected set; }

            public char[,] Grid { get; protected set; }
            public char[,] Moves { get; protected set; }
            public List<Ball> Balls { get; protected set; }

            public int BallsInHole { get; protected set; }

            public Map()
            {

            }

            public void ReadFromConsole()
            {
                string[] inputs = Console.ReadLine().Split(' ');

                Width = int.Parse(inputs[0]);
                Height = int.Parse(inputs[1]);

                Balls = new List<Ball>();
                Grid = new char[Width, Height];
                Moves = new char[Width, Height];
                for (int y = 0; y < Height; y++)
                {
                    string row = Console.ReadLine();
                    for(int x = 0; x < Width; x++)
                    {
                        if (int.TryParse(row[x].ToString(), out int shotCount))
                            Balls.Add(new Ball(x, y, shotCount));

                        Grid[x, y] = row[x];
                        Moves[x, y] = NONE;
                    }
                }
            }

            public bool IsCompleted()
            {
                return BallsInHole == Balls.Count;
            }

            public bool IsInside(int x, int y)
            {
                return 0 <= x && x < Width
                    && 0 <= y && y < Height;
            }

            public bool CanFlyBy(char cell)
            {
                return cell == NONE || cell == WATER;
            }

            public bool CanLand(char cell)
            {
                return cell == NONE || cell == HOLE;
            }

            public bool ApplyMove(Ball ball, Move move)
            {
                if (ball.ShotCount <= 0)
                    return false;

                int x = ball.X + (move.X * ball.ShotCount);
                int y = ball.Y + (move.Y * ball.ShotCount);

                if (!IsInside(x, y))
                    return false;

                if (!CanLand(Moves[x, y]) || !CanLand(Grid[x,y]))
                    return false;

                if (ball.ShotCount <= 1 && Grid[x, y] != HOLE)
                    return false;

                for (int i = 1; i < ball.ShotCount; i++)
                {
                    int xx = ball.X + (move.X * i);
                    int yy = ball.Y + (move.Y * i);
                    if (!CanFlyBy(Grid[xx, yy]) || !CanFlyBy(Moves[xx, yy]))
                        return false;
                }

                Console.Error.WriteLine(string.Format("#MOVE {0} to ({1},{2}) {3}*{4}", ball, x, y, move.C, ball.ShotCount));

                for (int i = 0; i < ball.ShotCount; i++)
                {
                    Moves[ball.X + (move.X * i), ball.Y + (move.Y * i)] = move.C;
                }

                if (Grid[x, y] == HOLE)
                {
                    BallsInHole += 1;
                    Grid[x, y] = HOLE_FILLED;
                }

                ball.Moves.Push(move);
                ball.X = x;
                ball.Y = y;
                ball.ShotCount -= 1;

                return true;
            }

            public void UndoMove(Ball ball)
            {
                if (ball.Moves.Count == 0)
                    return;

                if (Grid[ball.X, ball.Y] == HOLE_FILLED)
                {
                    BallsInHole -= 1;
                    Grid[ball.X, ball.Y] = HOLE;
                }

                Move move = ball.Moves.Pop();
                ball.ShotCount += 1;

                Console.Error.WriteLine(string.Format("#UNDO {0} to ({1},{2}) {3}*{4}", ball,
                    ball.X - (move.X * ball.ShotCount), ball.Y - (move.Y * ball.ShotCount), move.C, ball.ShotCount));

                for (int i = 0; i < ball.ShotCount; i++)
                {
                    Moves[ball.X - (move.X * i), ball.Y - (move.Y * i)] = NONE;
                }

                ball.X = ball.X - (move.X * ball.ShotCount);
                ball.Y = ball.Y - (move.Y * ball.ShotCount);

            }

            public bool BallInHole(int ball)
            {
                Ball b = Balls[ball];
                return b.ShotCount <= 0 || Grid[b.X, b.Y] == HOLE_FILLED || Grid[b.X, b.Y] == HOLE;
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                for (int y = 0; y < Height; y++)
                {
                    for (int x = 0; x < Width; x++)
                    {
                        sb.Append(Grid[x, y]);
                    }
                    sb.AppendLine();
                }

                return sb.Remove(sb.Length - Environment.NewLine.Length, Environment.NewLine.Length).ToString();
            }

            public string ToStringSolution()
            {
                StringBuilder sb = new StringBuilder();

                for (int y = 0; y < Height; y++)
                {
                    for (int x = 0; x < Width; x++)
                    {
                        sb.Append(Moves[x, y]);
                    }
                    sb.AppendLine();
                }

                return sb.Remove(sb.Length - Environment.NewLine.Length, Environment.NewLine.Length).ToString();
            }
        }

        public static void Main(string[] args)
        {
            Map map = new Map();
            map.ReadFromConsole();

            Console.Error.WriteLine(map);
            Console.Error.WriteLine();

            RecursiveMove(map, 0);

            Console.WriteLine(map.ToStringSolution());
        }

        static bool RecursiveMove(Map map, int ball)
        {
            if (map.Balls.Count < ball)
                return false;

            for (int move = 0; move < MOVES.Length; move++)
            {
                if(map.ApplyMove(map.Balls[ball], MOVES[move]))
                {
                    if (map.IsCompleted())
                        return true;

                    if (map.BallInHole(ball))
                    {
                        //Next ball
                        if (!RecursiveMove(map, ball + 1))
                            map.UndoMove(map.Balls[ball]);
                        else
                            return true;
                    }
                    else if(map.Balls[ball].ShotCount == 0)
                    {
                        //Undo and kepp trying
                        map.UndoMove(map.Balls[ball]);
                    }
                    else
                    {
                        //Next move, same ball
                        if (!RecursiveMove(map, ball))
                            map.UndoMove(map.Balls[ball]);
                        else
                            return true;
                    }
                }
            }

            return false;
        }
    }
}