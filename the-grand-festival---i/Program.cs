﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// <see cref="https://www.codingame.com/ide/puzzle/the-grand-festival---i"/>
/// 
/// Rules:
/// There are N tournaments in all, held from day 1 to day N.
/// Merry and Pippin can play at most R consecutive tournaments before they have to rest.
/// The prize money for all the tournaments will be given to you.
/// You need to display the maximum total prize money.
/// </summary>
class Solution
{
    class Progress
    {
        public int Position { get; set; }
        public bool[] Pauses { get; set; }

        public Progress(int position, bool value, bool[] pauses)
        {
            Position = position;
            Pauses = (bool[])pauses.Clone();
            Pauses[position] = value;
        }
    }

    static void Main(string[] args)
    {
#if TRACE
        Console.SetIn(new StringReader(
///*
@"20
3
12
13
11
14
16
12
15
9
7
18
8
20
31
14
16
12
18
9
10
22")); //--> 233
//*/

/*
@"7
3
13
12
11
9
16
17
100")); //--> 169
//*/

#endif

        int numTournaments = int.Parse(Console.ReadLine());
        int maxConsecutive = int.Parse(Console.ReadLine());

        int[] tourPrizes = new int[numTournaments];
        bool[] pauses = new bool[numTournaments];
        for (int i = 0; i < numTournaments; i++)
        {
            tourPrizes[i] = int.Parse(Console.ReadLine());
            pauses[i] = false;
        }

        int maxPrize = 0;

        Stack<Progress> stack = new Stack<Progress>();
        stack.Push(new Progress(0, true, pauses));
        stack.Push(new Progress(0, false, pauses));

        int checkCount = 0;
        int validCheckCount = 0;
        int prizeCheckCount = 0;

        while (stack.Count > 0)
        {
            Progress p = stack.Pop();
            checkCount++;

            if (IsValid(p.Position, p.Pauses, maxConsecutive))
            {
                validCheckCount++;

                if (p.Position >= (numTournaments - 1))
                {
                    prizeCheckCount++;

                    //Check the solution
                    int prize = CountPrizes(tourPrizes, p.Pauses);
                    maxPrize = Math.Max(prize, maxPrize);
                }
                else
                {
                    //Add next branchs
                    stack.Push(new Progress(p.Position + 1, true, p.Pauses));
                    stack.Push(new Progress(p.Position + 1, false, p.Pauses));
                }
            }
        }

        Console.Error.WriteLine("checkCount = {0}, validCheckCount = {1}, prizeCheckCount = {2}", checkCount, validCheckCount, prizeCheckCount);
        Console.WriteLine(maxPrize);

#if TRACE
        Console.WriteLine("\r\nPress any key...");
        Console.ReadKey();
#endif
    }

    public static int CountPrizes(int[] tourPrizes, bool[] pauses)
    {
        int prizes = 0;
        for(int i = 0; i < tourPrizes.Length; i++)
        {
            if (!pauses[i])
                prizes += tourPrizes[i];
        }

        return prizes;
    }

    public static bool IsValid(int position, bool[] pauses, int maxConsecutive)
    {
        int countConsecutive = 0;
        for(int i = 0; i < pauses.Length && i <= position; i++)
        {
            if (!pauses[i])
            {
                countConsecutive += 1;
                if (countConsecutive > maxConsecutive)
                    return false;
            }
            else
            {
                countConsecutive = 0;
            }
        }

        return true;
    }
}