﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// <see cref="https://www.codingame.com/ide/puzzle/encryptiondecryption-of-enigma-machine"/>
/// </summary>
class Solution
{
    static void Main(string[] args)
    {
#if TRACE
/*
        string test =
@"ENCODE
4
BDFHJLCPRTXVZNYEIWGAKMUSQO
AJDKSIRUXBLHWTMCQGZNPYFVOE
EKMFLGDQVZNTOWYHXUSPAIBRCJ
AAA";
//*/

/*
        string test =
@"DECODE
9
BDFHJLCPRTXVZNYEIWGAKMUSQO
AJDKSIRUXBLHWTMCQGZNPYFVOE
EKMFLGDQVZNTOWYHXUSPAIBRCJ
PQSACVVTOISXFXCIAMQEM";
//*/

/*
        string test =
@"DECODE
4
BDFHJLCPRTXVZNYEIWGAKMUSQO
AJDKSIRUXBLHWTMCQGZNPYFVOE
EKMFLGDQVZNTOWYHXUSPAIBRCJ
KQF";
//*/

///*
        string test =
        @"DECODE
5
BDFHJLCPRTXVZNYEIWGAKMUSQO
AJDKSIRUXBLHWTMCQGZNPYFVOE
EKMFLGDQVZNTOWYHXUSPAIBRCJ
XPCXAUPHYQALKJMGKRWPGYHFTKRFFFNOUTZCABUAEHQLGXREZ";
//*/
        Console.SetIn(new StringReader(test));
#endif

        string ROTOR0 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string[] ROTORS = new string[3];

        string operation = Console.ReadLine();
        int pseudoRandomNumber = int.Parse(Console.ReadLine());
        for (int i = 0; i < 3; i++)
            ROTORS[i] = Console.ReadLine();

        string message = Console.ReadLine();

        if (operation == "ENCODE")
        {
            message = ApplyCaesarShift(message, pseudoRandomNumber, ROTOR0);
            foreach (string rotor in ROTORS)
                message = MapRotor(message, ROTOR0, rotor);
        }
        else
        {
            for(int i = ROTORS.Length - 1; i >= 0; i--)
                message = UndoMapRotor(message, ROTOR0, ROTORS[i]);
            message = UndoCaesarShift(message, pseudoRandomNumber, ROTOR0);
        }

        Console.WriteLine(message);

#if TRACE
        Console.WriteLine("\r\nPress any key...");
        Console.ReadKey();
#endif
    }

    static int IndexOf(string rotor, char character)
    {
        //FIXME: too dirty (but working)
        for(int i = 0; i < rotor.Length; i++)
        {
            if (rotor[i] == character)
                return i;
        }

        return -1;
    }

    static string ApplyCaesarShift(string message, int pseudoRandomNumber, string ROTOR0)
    {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < message.Length; i++)
        {
            char character = message[i];
            int character_index = IndexOf(ROTOR0, character);

            character_index = (character_index + i + pseudoRandomNumber) % ROTOR0.Length;
            sb.Append(ROTOR0[character_index]);
        }

        return sb.ToString();
    }

    static string UndoCaesarShift(string message, int pseudoRandomNumber, string ROTOR0)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < message.Length; i++)
        {
            char character = message[i];
            int character_index = IndexOf(ROTOR0, character);

            character_index = (character_index - i - pseudoRandomNumber);
            while (character_index < 0)
                character_index += ROTOR0.Length;
            character_index = character_index % ROTOR0.Length;
            sb.Append(ROTOR0[character_index]);
        }

        return sb.ToString();
    }

    static string MapRotor(string message, string ROTOR0, string rotor)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < message.Length; i++)
        {
            char character = message[i];
            int character_index = IndexOf(ROTOR0, character);

            sb.Append(rotor[character_index]);
        }

        return sb.ToString();
    }

    static string UndoMapRotor(string message, string ROTOR0, string rotor)
    {
        return MapRotor(message, rotor, ROTOR0);
    }

}