﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace nonogram_inversor
{
    public class Solution
    {
        public enum State
        {
            NONE = ' ', BLACK = 'X', WHITE = 'O'
        }

        public class Section
        {
            public int Start { get; set; }
            public int End { get; set; }
            public int Length { get; set; }

            public Section()
            {
                Start = End = Length = 0;
            }

            public Section(int start, int end)
            {
                Start = start;
                End = end;
                Length = end - start;
            }
        }

        public class Group
        {
            public int Number { get; protected set; }
            public bool Done { get; set; }

            public Group(int number)
            {
                Number = number;
                Done = false;
            }
        }

        public class GroupList
        {
            public List<Group> Groups {get; protected set; }
            public int LengthNeeded { get; protected set; }
            public int Count { get { return Groups.Count; } }

            public GroupList()
            {
                Groups = new List<Group>();
            }

            public void Add(Group group)
            {
                Groups.Add(group);

                LengthNeeded += group.Number;
                if (Groups.Count > 0)
                    LengthNeeded += 1;
            }

            public Group this[int index]
            {
                get { return Groups[index]; }
                set { Groups[index] = value; }
            }
        }

        public class Nonogram
        {
            public int Width { get; protected set; }
            public int Height { get; protected set; }
            public State[,] Grid { get; protected set; }
            public GroupList[] Horizontal { get; protected set; }
            public GroupList[] Vertical { get; protected set; }

            public void ReadFromConsole()
            {
                string[] inputs = Console.ReadLine().Split(' ');
                Width = int.Parse(inputs[0]);
                Height = int.Parse(inputs[1]);

                Grid = new State[Width, Height];
                for (int x = 0; x < Width; x++)
                    for (int y = 0; y < Height; y++)
                        Grid[x, y] = State.NONE;

                Horizontal = new GroupList[Width];
                for (int x = 0; x < Width; x++)
                {
                    string[] blackGroups = Console.ReadLine().Split(' ');
                    Horizontal[x] = new GroupList();
                    for (int j = 0; j < blackGroups.Length; j++)
                        Horizontal[x].Add(new Group(int.Parse(blackGroups[j])));
                }

                Vertical = new GroupList[Height];
                for (int y = 0; y < Height; y++)
                {
                    string[] blackGroups = Console.ReadLine().Split(' ');
                    Vertical[y] = new GroupList();
                    for (int j = 0; j < blackGroups.Length; j++)
                        Vertical[y].Add(new Group(int.Parse(blackGroups[j])));
                }
            }

            protected void CalculateSections(out List<Section>[] hSections, out List<Section>[] vSections)
            {
                int sectionStart = -1;
                State lastState = State.NONE;
                hSections = new List<Section>[Width];
                for (int y = 0; y < Height; y++)
                {
                    int blackCells = 0;
                    for (int x = 0; x < Width; x++)
                    {
                        State state = Grid[x, y];
                        if (state == State.BLACK)
                            blackCells += 1;

                        
                    }
                }

                vSections = new List<Section>[Height];
                for (int x = 0; x < Width; x++)
                {

                }
            }

            public void Resolve()
            {
                bool someMove = false;

                List<Section>[] HSections = new List<Section>[Width];
                List<Section>[] VSections = new List<Section>[Height];

                do
                {



                    for (int y = 0; y < Height; y++)
                    {
                        
                    }


                    for(int x = 0; x < Width; x++)
                    {

                    }


                } while (someMove);
            }

            public void Invert()
            {
                for (int y = 0; y < Height; y++)
                {
                    for (int x = 0; x < Width; x++)
                    {
                        if (Grid[x, y] == State.BLACK)
                            Grid[x, y] = State.WHITE;
                        else if (Grid[x, y] == State.WHITE)
                            Grid[x, y] = State.BLACK;
                    }
                }

                const State LOOKING_STATE = State.BLACK;
                for (int y = 0; y < Height; y++)
                {
                    State lastCell = State.NONE;
                    List<int> adjacent = new List<int>();
                    int cont = 0;
                    for (int x = 0; x < Width; x++)
                    {
                        if(Grid[x,y] == LOOKING_STATE)
                        {
                            cont += 1;
                        }
                        else if(lastCell == LOOKING_STATE && Grid[x,y] != LOOKING_STATE)
                        {
                            adjacent.Add(cont);
                            cont = 0;
                        }

                        lastCell = Grid[x, y];
                    }

                    if(lastCell == LOOKING_STATE)
                        adjacent.Add(cont);

                    if (adjacent.Count == 0)
                        adjacent.Add(0);

                    Vertical[y] = new GroupList();
                    foreach (int adjNumber in adjacent)
                        Vertical[y].Add(new Group(adjNumber));
                }

                for (int x = 0; x < Width; x++) 
                {
                    State lastCell = State.NONE;
                    List<int> adjacent = new List<int>();
                    int cont = 0;
                    for (int y = 0; y < Height; y++)
                    {
                        if (Grid[x, y] == LOOKING_STATE)
                        {
                            cont += 1;
                        }
                        else if (lastCell == LOOKING_STATE && Grid[x, y] != LOOKING_STATE)
                        {
                            adjacent.Add(cont);
                            cont = 0;
                        }

                        lastCell = Grid[x, y];
                    }

                    if (lastCell == LOOKING_STATE)
                        adjacent.Add(cont);

                    if (adjacent.Count == 0)
                        adjacent.Add(0);

                    Horizontal[x] = new GroupList();
                    foreach (int adjNumber in adjacent)
                        Horizontal[x].Add(new Group(adjNumber));
                }
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("Nonogram ({0}, {1})", Width, Height);
                sb.AppendLine();

                for (int y = 0; y < Height; y++)
                {
                    for (int x = 0; x < Width; x++)
                    {
                        sb.Append((char) Grid[x,y]);
                    }
                    sb.AppendLine();
                }

                return sb.ToString();
            }

            public string ToSolutionString()
            {
                StringBuilder sb = new StringBuilder();
                for (int x = 0; x < Width; x++)
                {
                    sb.Append(Horizontal[x][0]);
                    for (int i = 1; i < Horizontal[x].Count; i++)
                        sb.Append(" ").Append(Horizontal[x][i]);
                    sb.AppendLine();
                }

                for (int y = 0; y < Height; y++)
                {
                    sb.Append(Vertical[y][0]);
                    for (int i = 1; i < Horizontal[y].Count; i++)
                        sb.Append(" ").Append(Horizontal[y][i]);
                    sb.AppendLine();
                }

                return sb.Remove(sb.Length - Environment.NewLine.Length, Environment.NewLine.Length).ToString();
            }
        }

        public static void Main(string[] args)
        {
            Nonogram nonogram = new Nonogram();
            nonogram.ReadFromConsole();
            nonogram.Resolve();
            nonogram.Invert();
            Console.Write(nonogram.ToSolutionString());
        }
    }
}