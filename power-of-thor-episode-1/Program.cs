﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 * ---
 * Hint: You can use the debug stream to print initialTX and initialTY, if Thor seems not follow your orders.
 **/
class Player
{
    static void Main(string[] args)
    {
        string[] inputs = Console.ReadLine().Split(' ');
        int[] target = new int[] { int.Parse(inputs[0]), int.Parse(inputs[1]) };
        int[] pos = new int[] { int.Parse(inputs[2]), int.Parse(inputs[3]) };
        int[] delta = new int[] { 0, 0 };

        //Console.Error.WriteLine("From (%d, %d) to (%d, %d)", pos[0], pos[1], target[0], target[1]);

        // game loop
        while (target[0] != pos[0] && target[1] != pos[1])
        {
            int remainingTurns = int.Parse(Console.ReadLine()); // The remaining amount of turns Thor can move. Do not remove this line.
            string output = "";

            delta[1] = target[1] - pos[1];
            if (delta[1] < 0)
            {
                --pos[1];
                output += "N";
            }
            else if (delta[1] > 0)
            {
                ++pos[1];
                output += "S";
            }

            delta[0] = target[0] - pos[0];
            if (delta[0] < 0)
            {
                --pos[0];
                output += "W";
            }
            else if (delta[0] > 0)
            {
                ++pos[0];
                output += "E";
            }

            Console.WriteLine(output);
            Console.Error.WriteLine(output + " -- from (" + pos[0] + ", " + pos[1] + ") to (" + target[0] + ", " + target[1] + ")");
        }
    }
}