﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BarcodeScanner
{
    class Program
    {
        private static readonly string[] LEFT_PART_CODES =
        {
            "LLLLLL",
            "LLGLGG",
            "LLGGLG",
            "LLGGGL",
            "LGLLGG",
            "LGGLLG",
            "LGGGLL",
            "LGLGLG",
            "LGLGGL",
            "LGGLGL"
        };
        private static readonly Dictionary<string, int> DIC_LEFT_PART_CODES = new Dictionary<string, int>();

        private static readonly string RIGHT_PART_CODES = "RRRRRR";

        private static readonly string[] L_CODES =
        {
            "0001101",
            "0011001",
            "0010011",
            "0111101",
            "0100011",
            "0110001",
            "0101111",
            "0111011",
            "0110111",
            "0001011"
        };
        private static readonly Dictionary<string, int> DIC_L_CODES = new Dictionary<string, int>();
        
        private static readonly string[] R_CODES =
        {
            "1110010",
            "1100110",
            "1101100",
            "1000010",
            "1011100",
            "1001110",
            "1010000",
            "1000100",
            "1001000",
            "1110100"
        };
        private static readonly Dictionary<string, int> DIC_R_CODES = new Dictionary<string, int>();

        private static readonly string SIDE_GUARD = "101"; //LEFT AND RIGHT
        private static readonly string CENTRAL_GUARD = "01010";

        private static readonly string INVALID_SCAN = "INVALID SCAN";

        private static string Reverse(string str)
        {
            StringBuilder sb = new StringBuilder();
            for(int i = str.Length - 1; i >= 0; i--)
            {
                sb.Append(str[i]);
            }
            return sb.ToString();
        }

        private static void InitDictionaries()
        {
            for(int i = 0; i < LEFT_PART_CODES.Length; i++)
            {
                DIC_LEFT_PART_CODES.Add(LEFT_PART_CODES[i], i);
            }
            for (int i = 0; i < L_CODES.Length; i++)
            {
                DIC_L_CODES.Add(L_CODES[i], i);
                DIC_L_CODES.Add(Reverse(L_CODES[i]), i);
            }
            for (int i = 0; i < R_CODES.Length; i++)
            {
                DIC_R_CODES.Add(R_CODES[i], i);
                DIC_R_CODES.Add(Reverse(R_CODES[i]), i);
            }
        }


#if TRACE
        private static readonly string[] _test = {
        /*0*/    "10100011010100111011110100010110010111001110101010110011010011101001110101110010010001010000101",
        /*1*/   "10101101110011011000110100011010110011011001101010110011010010001001110100100011100101101100101",
        /*2*/   "10100110010010011010000101000110111001000010101010100010010010001110100111001011001101101100101",
        /*3*/   "10100010010011101001000100010010000101001110101010100110010100001101000100111010010001101110101",
        /*4*/   "11100011010111011001001101100010011001001001101010110110010000101001110111010011100101011100101",
        /*5*/   "10100101110010111010000100101110011011010011101010101000011101101011110110011011001101011000101", //reversed
        /*6*/   "10100110010010011010000101000110111001000010101010100010010010001110100111001011001101101100101", //wrong chechsum
        /*7*/   "11100011010111011001001101100010011001001001101010110110010000101001110111010011100101011100101", //broken guards
        /*8*/   "10101011110011011001001101100110001101010011101010100100011001101001110110010010111001101100101", //broken parity
        };
        private static readonly string[] _solution = {
            "4003994155486",
            "4820011185802",
            "INVALID SCAN",
            "9785961468748",
            "INVALID SCAN",
            "5011386029399",
            "INVALID SCAN",
            "INVALID SCAN",
            "INVALID SCAN",
        };
#endif

        static void Main(string[] args)
        {
#if TRACE
            int testi = 9;
            Console.SetIn(new StringReader(_test[testi]));
#endif
            InitDictionaries();

            string solution = "";
            string scanline = Console.ReadLine();

            try
            {
                int cursor = 0;

                Check(scanline, SIDE_GUARD, ref cursor);

                string leftPart = Decode6_7bit(scanline, false, out string leftCodes, ref cursor);

                Check(scanline, CENTRAL_GUARD, ref cursor);

                bool reversed = leftCodes == RIGHT_PART_CODES;

                string rightPart = Decode6_7bit(scanline, reversed, out string rightCodes, ref cursor);

                Check(scanline, SIDE_GUARD, ref cursor);

                if(reversed)
                {
                    //Reverse!
                    string aux = Reverse(leftPart);
                    leftPart = Reverse(rightPart);
                    rightPart = aux;

                    aux = Reverse(leftCodes);
                    leftCodes = Reverse(rightCodes);
                    rightCodes = aux;
                }
                else
                {
                    leftCodes = leftCodes.Replace("R", "G");
                }

                if(!DIC_LEFT_PART_CODES.ContainsKey(leftCodes))
                    throw new ArgumentException(string.Format("Invalid left code \"{0}\"", leftCodes));

                if(rightCodes != RIGHT_PART_CODES)
                    throw new ArgumentException(string.Format("Invalid right code \"{0}\"", rightCodes));

                solution = DIC_LEFT_PART_CODES[leftCodes] + leftPart + rightPart;
                CheckSum(solution);
            }
            catch(ArgumentException ex)
            {
                Console.Error.WriteLine(ex.Message);
                solution = INVALID_SCAN;
            }

            Console.WriteLine(solution);

#if TRACE
            if (solution == _solution[testi])
                Console.Error.WriteLine("OK!");
            else
                Console.Error.WriteLine("ERROR!");

            Console.ReadKey();
#endif
        }

        private static bool Check(string scanline, string checkStr, ref int cursor)
        {
            for(int i = 0; i < checkStr.Length; i++)
            {
                if (scanline[cursor + i] != checkStr[i])
                    throw new ArgumentException(string.Format("Not found \"{0}\" in position {1}", checkStr, cursor));
            }

            cursor += checkStr.Length;
            return true;
        }

        private static string Decode6_7bit(string scanline, bool reversed, out string codes, ref int cursor)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbCodes = new StringBuilder();
            for(int i = 0; i < 6; i++)
            {
                string part = scanline.Substring(cursor, 7);

                if(DIC_R_CODES.ContainsKey(part))
                {
                    sb.Append(DIC_R_CODES[part]);
                    sbCodes.Append(reversed ? "G" : "R");
                }
                else if (DIC_L_CODES.ContainsKey(part))
                {
                    sb.Append(DIC_L_CODES[part]);
                    sbCodes.Append("L");
                }
                else
                {
                    throw new ArgumentException(string.Format("Part \"{0}\" does not match any number. Cursor={1}, i={2}", part, cursor, i));
                }

                cursor += 7;
            }

            codes = sbCodes.ToString();
            return sb.ToString();
        }

        private static void CheckSum(string barcode)
        {
            //The explanation is not understood
            //found: https://en.wikipedia.org/wiki/International_Article_Number
            //found: https://en.wikipedia.org/wiki/International_Standard_Book_Number#ISBN-13_check_digit_calculation
            //code found: https://es.wikipedia.org/wiki/European_Article_Number#C#

            int iDigit;
            int iSum = 0;
            int iSumOdd = 0;

            for (int i = 0; i < barcode.Length; i++)
            {
                iDigit = Convert.ToInt32(barcode.Substring(i, 1));
                if (i % 2 != 0)
                {
                    iSumOdd += iDigit;
                }
                else
                {
                    iSum += iDigit;
                }
            }

            iDigit = (iSumOdd * 3) + iSum;

            int iCheckSum = iDigit % 10; //(10 - (iDigit % 10)) % 10;
            //Console.Write("Digito de control: " + iCheckSum.ToString());
            if (iCheckSum % 10 != 0)
                throw new ArgumentException(string.Format("Invalid checksum {0} for barcode {1}", iCheckSum, barcode));
        }
    }
}
