﻿using System;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace tester
{
    [TestClass]
    public class WinamaxSponsoredContest
    {
        /// <summary>
        /// Test Inputs and Outputs
        /// </summary>
        private readonly static string[] TEST_CASES = new string[]
        {
@"2 1
1H", ">.",
        };

        [TestMethod]
        public void Test1()
        {
            test(TEST_CASES[0], TEST_CASES[1]);
        }

        private void test(string input, string output)
        {
            StringBuilder sbOutput = new StringBuilder();
            Console.SetOut(new StringWriter(sbOutput));
            Console.SetIn(new StringReader(input));

            winamax_sponsored_contest.

            Assert.AreEqual(output, sbOutput.ToString());
        }
    }
}
