﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

class Solution
{
    class Progress
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Value { get; set; }

        public Progress() { }
        public Progress(int x, int y, int value)
        {
            X = x;
            Y = y;
            Value = value;
        }
        public Progress(Progress other)
        {
            X = other.X;
            Y = other.Y;
            Value = other.Value;
        }

        public override string ToString()
        {
            return string.Format("({0}, {1}) {2}", X, Y, Value);
        }
    }

    static void Main(string[] args)
    {
#if TRACE
        //Test case
        string test =
@"120070560
507932080
000001000
010240050
308000402
070085010
000700000
080423701
034010028";

        Console.SetIn(new StringReader(test));
#endif
        //Init board
        int[,] sudokuBoard = new int[9, 9];
        bool[,] boardLocks = new bool[9, 9];
        int placedNumbers = 0;
        int maxPlaced = 9 * 9;

        for (int y = 0; y < 9; y++)
        {
            string line = Console.ReadLine();
            for (int x = 0; x < 9; x++)
            {
                sudokuBoard[x, y] = int.Parse(line[x].ToString());
                boardLocks[x, y] = (sudokuBoard[x, y] != 0);
                if (boardLocks[x, y])
                    placedNumbers += 1;
            }
        }

        //Resolve board
        Stack<Progress> stack = new Stack<Progress>();
        Progress current = new Progress(0, 0, 1);
        while(current != null)
        {
            if(boardLocks[current.X, current.Y])
            {
                //Locked position, try move to next (or go back)
                Progress lastCurrent = current;
                current = NextPosition(current);
                if(current == null && placedNumbers < maxPlaced)
                {
                    placedNumbers -= 1;
                    sudokuBoard[lastCurrent.X, lastCurrent.Y] = 0;
                    current = stack.Pop();
                }
            }
            else
            {
                if(IsValidSector(sudokuBoard, current) &&
                    IsValidLines(sudokuBoard, current))
                {
                    //Is valid to place value
                    placedNumbers += 1;
                    stack.Push(current);
                    sudokuBoard[current.X, current.Y] = current.Value;
                    current = NextPosition(current);
                }
                else
                {
                    //Is not valid, try next value (or go back)
                    current.Value += 1;
                    if (current.Value > 9)
                    {
                        placedNumbers -= 1;
                        sudokuBoard[current.X, current.Y] = 0;
                        current = stack.Pop();
                    }
                }
            }
        }

        //Print solution
        for (int y = 0; y < 9; y++)
        {
            for (int x = 0; x < 9; x++)
                Console.Write(sudokuBoard[x, y]);
            Console.WriteLine();
        }

#if TRACE
        Console.WriteLine("\r\nPress any key...");
        Console.ReadKey();
#endif
    }

    static string PrintBoard(int[,] sudokuBoard)
    {
        StringBuilder sb = new StringBuilder();
        for (int y = 0; y < 9; y++)
        {
            for (int x = 0; x < 9; x++)
                sb.Append(sudokuBoard[x, y]);
            sb.AppendLine();
        }
        return sb.ToString();
    }

    static Progress NextPosition(Progress progress)
    {
        Progress ret = new Progress(progress);
        ret.Value = 1;
        ret.X += 1;
        if(ret.X >= 9)
        {
            ret.X = 0;
            ret.Y += 1;
        }

        if (ret.Y >= 9)
            return null;
        return ret;
    }

    static bool IsValidSector(int[,] sudokuBoard, Progress progress)
    {
        int xSec1 = (progress.X / 3) * 3;
        int xSec2 = xSec1 + 3;
        int ySec1 = (progress.Y / 3) * 3;
        int ySec2 = ySec1 + 3;

        bool[] found = new bool[10];
        found[progress.Value] = true;
        for (int y = ySec1; y < ySec2; y++)
        {
            for (int x = xSec1; x < xSec2; x++)
            {
                if (sudokuBoard[x, y] != 0 && found[sudokuBoard[x, y]])
                    return false;
                else
                    found[sudokuBoard[x, y]] = true;
            }
        }

        return true;
    }

    static bool IsValidLines(int[,] sudokuBoard, Progress progress)
    {
        //Vertical
        bool[] found = new bool[10];
        found[progress.Value] = true;
        for (int y = 0; y < 9; y++)
        {
            if (sudokuBoard[progress.X, y] != 0 && found[sudokuBoard[progress.X, y]])
                return false;
            else
                found[sudokuBoard[progress.X, y]] = true;
        }

        //Horizontal
        found = new bool[10];
        found[progress.Value] = true;
        for (int x = 0; x < 9; x++)
        {
            if (sudokuBoard[x, progress.Y] != 0 && found[sudokuBoard[x, progress.Y]])
                return false;
            else
                found[sudokuBoard[x, progress.Y]] = true;
        }

        return true;
    }
}