﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player
{
    static void Main(string[] args)
    {
        int i1 = int.Parse(Console.ReadLine());
        int i2 = int.Parse(Console.ReadLine());
        int i3 = int.Parse(Console.ReadLine());

        Console.Error.WriteLine("Input {0}, {1}, {2}", i1, i2, i3);
        Console.Error.WriteLine();

        string solution = "B";



        // game loop
        int cont = 0;
        while (true)
        {
            string s1 = Console.ReadLine();
            string s2 = Console.ReadLine();
            string s3 = Console.ReadLine();
            string s4 = Console.ReadLine();

            Console.Error.WriteLine("Turn {0:000}), {1}, {2}, {3}, {4}", cont, s1, s2, s3, s4);

            for (int i = 0; i < i3; i++)
            {
                string[] inputs = Console.ReadLine().Split(' ');
                int s5 = int.Parse(inputs[0]);
                int s6 = int.Parse(inputs[1]);

                Console.Error.WriteLine("  | {0}, {1}", s5, s6);
            }

            Console.WriteLine(solution);
            cont += 1;
        }
    }
}