﻿using System;
using System.Collections.Generic;
using System.IO;

namespace bender_episode_1
{
    class Program
    {
        /** CHARACTER INFO:
        *  # - unbreakable wall
        *  X - breakable wall
        *  @ - start point
        *  $ - end point
        *  S, E, N, W - direction modifier
        *  B - breaker mode (toggle)
        *  I - inverted mode (toggle)
        *  T - teleport
        *  */
        class Map
        {
            public int x, y;
            public char[][] map;

            public Map(int x, int y)
            {
                this.x = x;
                this.y = y;
                map = new char[x][];
                for (int i = 0; i < x; i++)
                {
                    map[i] = new char[y];
                }
            }

            public char get(Pos pos)
            {
                return get(pos.x, pos.y);
            }

            public char get(int x, int y)
            {
                return map[x][y];
            }

            public void set(Pos pos, char c)
            {
                set(pos.x, pos.y, c);
            }

            public void set(int x, int y, char c)
            {
                map[x][y] = c;
            }
        }

        class Pos
        {
            public int x, y;

            public Pos() { }
            public Pos(int x, int y) { this.x = x; this.y = y; }
            public Pos(Pos p1, Pos p2)
            {
                x = p1.x + p2.x;
                y = p1.y + p2.y;
            }

            public Pos add(Pos p)
            {
                x += p.x;
                y += p.y;
                return this;
            }

            public override string ToString()
            {
                return String.Format("Pos({0}, {1})", x, y);
            }
        }

        class State
        {
            public bool inverted = false;
            public bool breaker = false;
            public char direction = 'S';
            public Pos pos;

            public override string ToString()
            {
                return String.Format("State(Pos={0}, Dir={1}, I={2}, B={3})", pos, direction, inverted, breaker);
            }
        }

        static void Main(string[] args)
        {
#if DEBUG
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.SetIn(new StringReader(
//@"10 10
//##########
//#        #
//#  S   W #
//#        #
//#  $     #
//#        #
//#@       #
//#        #
//#E     N #
//##########"));
//@"5 5
//#####
//#@  #
//#   #
//#  $#
//#####"));
//@"10 10
//##########
//#    T   #
//#        #
//#        #
//#        #
//#@       #
//#        #
//#        #
//#    T  $#
//##########"));
//LOOP
@"15 15
###############
#      IXXXXX #
#  @          #
#E S          #
#             #
#  I          #
#  B          #
#  B   S     W#
#  B   T      #
#             #
#         T   #
#         B   #
#N          W$#
#        XXXX #
###############"));
#endif
            State state = new State();
            LinkedList<string> moves = new LinkedList<string>();

            string[] inputs = Console.ReadLine().Split(' ');
            int l = int.Parse(inputs[0]);
            int c = int.Parse(inputs[1]);
            Map map = new Map(l, c);
            
            for (int x = 0; x < map.x; x++)
            {
                string row = Console.ReadLine();
                for(int y = 0; y < map.y; y++)
                {
                    map.set(x, y, row[y]);
                    if (row[y] == '@')
                    {
                        state.pos = new Pos(x, y);
                    }
                }
            }

            print(map, state);

            int count = 0;
            string aux;
            while ((aux = move(map, state)) != null)
            {
                moves.AddLast(aux);

                Console.Error.Write("MOVE {0:0000} ", count++);
                Console.Error.WriteLine(aux);
                print(map, state);
                //Console.ReadKey();

                if(count > 500)
                {
                    Console.WriteLine("LOOP");
                    return;
                }
            }

            foreach (string move in moves)
            {
                Console.WriteLine(move);
            }
        }

        
        private static Pos delta(char dir)
        {
            switch(dir)
            {
                //Coordinates are inverted, wtf? x <-> y
                case 'S': return new Pos(1, 0);
                case 'E': return new Pos(0, 1);
                case 'N': return new Pos(-1, 0);
                case 'W': return new Pos(0, -1);
                default: throw new ArgumentException("Invalid dir=" + dir);
            }
        }

        /// <summary>
        /// TURN PRIORITY (not inverted)
        ///     S, E, N, W
        /// </summary>
        /// <param name="inverted"></param>
        /// <returns></returns>
        private static char nextDir(int count, bool inverted)
        {
            switch (count)
            {
                case 0: return inverted ? 'W' : 'S';
                case 1: return inverted ? 'N' : 'E';
                case 2: return inverted ? 'E' : 'N';
                case 3: return inverted ? 'S' : 'W';
                default: throw new ArgumentException("Invalid count=" + count);
            }
        }

        private static Pos nextPos(State state)
        {
            return delta(state.direction).add(state.pos);
        }

        private static string translate(char dir)
        {
            switch (dir)
            {
                case 'S': return "SOUTH";
                case 'E': return "EAST";
                case 'N': return "NORTH";
                case 'W': return "WEST";
                default: throw new ArgumentException("Invalid dir=" + dir);
            }
        }

        private static Pos findTeleport(Map map, Pos pos)
        {
            for(int x = 0; x < map.x; x++)
            {
                for(int y = 0; y < map.y; y++)
                {
                    char c = map.get(x, y);
                    if (c == 'T' && (pos.x != x || pos.y != y))
                        return new Pos(x, y);
                }
            }

            throw new ArgumentException("Not found teleport");
        }

        private static string move(Map map, State state)
        {
            char current = map.get(state.pos);

            switch(current)
            {
                case '$': return null; //end
                case 'S':
                case 'E':
                case 'N':
                case 'W':
                    state.direction = current;
                    break;
                case 'B':
                    state.breaker = !state.breaker;
                    break;
                case 'I':
                    state.inverted = !state.inverted;
                    break;
                case 'T':
                    state.pos = findTeleport(map, state.pos);
                    break;
                case 'X':
                    map.set(state.pos, ' ');
                    break;
            }

            Pos np = nextPos(state);
            char next = map.get(np);

            if (next == '#' || (next == 'X' && !state.breaker))
            {
                for(int i = 0; i < 4; i++)
                {
                    char nd = nextDir(i, state.inverted);
                    np = delta(nd).add(state.pos);
                    next = map.get(np);

                    Console.Error.WriteLine("CHECK {0}, found {1}", nd, next);

                    if (!(next == '#' || (next == 'X' && !state.breaker)))
                    {
                        state.direction = nd;
                        state.pos = np;
                        return translate(state.direction);
                    }
                }

                return null;
            }
            else
            {
                Console.Error.WriteLine("MOVE FORWARD");
                state.pos = np;
                return translate(state.direction);
            }
        }
       
        private static void print(Map map, State state)
        {
            if (state.inverted)
                Console.Error.WriteLine("INVERTED!");
            if(state.breaker)
                Console.Error.WriteLine("BREAKER!");

            Console.Error.Write("MAP");

            if(state != null)
            {
                Console.Error.Write(" pos: {0}, {1} [{2}]", state.pos.x, state.pos.y, map.get(state.pos));
            }

            Console.Error.WriteLine();

            for (int i = 0; i < map.x; i++)
            {
                for(int j = 0; j < map.y; j++)
                {
                    if (state != null && state.pos.x == i && state.pos.y == j)
                    {
                        switch (state.direction)
                        {
                            case 'N': Console.Error.Write("↑"); break;
                            case 'S': Console.Error.Write("↓"); break;
                            case 'E': Console.Error.Write("→"); break;
                            case 'W': Console.Error.Write("←"); break;
                            default: Console.Error.Write(" "); break;
                        }
                    }
                    else
                    {
                        Console.Error.Write(map.get(i, j));
                    }
                }
                Console.Error.WriteLine();
            }
        }
    }
}
